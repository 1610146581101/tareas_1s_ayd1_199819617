# Servidor API REST con Docker

Este es un servidor API REST básico en JavaScript que devuelve un nombre y un número de carne. Se desplegará dentro de un contenedor Docker.

## Despliegue con Docker

1. Clona este repositorio o descarga los archivos en tu máquina local.

2. Asegúrate de tener Docker instalado en tu sistema.

3. Abre una terminal y navega hasta la carpeta del proyecto.

4. Construye la imagen de Docker ejecutando el siguiente comando:
   1. docker build -t mi-servidor-api .
   
5. Una vez que la imagen se haya construido con éxito, puedes ejecutar el contenedor con el siguiente comando:
   1. docker run -d -p 3000:3000 mi-servidor-api
   
6. Esto ejecutará el servidor en el puerto 3000 de tu máquina local.

## Llamada de Petición y Respuesta

7. Puedes realizar una petición GET al servidor para obtener el nombre y el número de carne.

    **Petición:**
        GET http://localhost:3000/

    **Respuesta:**

    ```json
        {
        "nombre": "Juan Pérez",
        "carne": "20201234",
        "MejorAuxiliar": "el mejor aux"
        }
    ```
8. Ahora puedes construir y ejecutar el contenedor Docker siguiendo las instrucciones de este archivo.
   
## Imagenes de prueba
**Ejecución del servidor:**    
[![Running-Server.png](https://i.postimg.cc/Bb4ykDMg/Running-Server.png)](https://postimg.cc/vg2361Rg)


**Respuesta del servidor:**
[![peticion-servidor.png](https://i.postimg.cc/mD4YRSM0/peticion-servidor.png)](https://postimg.cc/p90hJzsZ)