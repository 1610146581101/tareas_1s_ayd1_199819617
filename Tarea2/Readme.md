# Tarea 2: <a href="https://mcdonalds.com.gt/">Página de Mcdonals </a>

## Requerimientos Funcionales:

* Registro de Usuario: Los usuarios deben poder registrarse en la plataforma para acceder a características personalizadas, como historial de pedidos y promociones.

* Catálogo de Productos: Mostrar de manera clara y organizada todos los productos disponibles, con detalles sobre precios, descripciones y opciones de personalización.

* Carrito de Compras: Permitir a los usuarios agregar productos a un carrito de compras, revisar el contenido y realizar fácilmente el proceso de pago.

* Proceso de Pedido: Facilitar la realización de pedidos con un proceso intuitivo y eficiente, proporcionando opciones de entrega o recogida en la tienda.

* Sistema de Pago Seguro: Garantizar transacciones seguras mediante la integración con pasarelas de pago confiables.

* Sistema de Comentarios y Calificaciones: Permitir a los usuarios dejar comentarios y calificaciones sobre los productos y el servicio.

* Localizador de Tiendas: Proporcionar un sistema de búsqueda que permita a los usuarios encontrar la ubicación de las tiendas más cercanas.

* Ofertas y Promociones: Mostrar ofertas especiales, promociones y descuentos para incentivar a los clientes a realizar pedidos.

* Soporte Multilingüe: Ofrecer el sitio en varios idiomas para llegar a un público más amplio.

* Integración con Redes Sociales: Permitir a los usuarios compartir sus pedidos o experiencias en redes sociales.

## Requerimientos No Funcionales:

* Tiempo de Carga: Garantizar un tiempo de carga rápido para mejorar la experiencia del usuario.

* Escalabilidad: Asegurar que el sistema pueda manejar un aumento en la carga de usuarios sin pérdida de rendimiento.

* Seguridad: Implementar medidas de seguridad sólidas para proteger la información del usuario y las transacciones.

* Compatibilidad con Dispositivos: Asegurar que el sitio sea compatible con una amplia gama de dispositivos y navegadores.

* Disponibilidad: Garantizar la disponibilidad del sitio web, minimizando el tiempo de inactividad.

* Accesibilidad: Cumplir con estándares de accesibilidad para garantizar que el sitio sea utilizable por personas con discapacidades.

* Resiliencia: Implementar medidas para recuperarse rápidamente de fallos o interrupciones del servicio.

* Usabilidad: Diseñar una interfaz de usuario intuitiva y fácil de usar.

* Rendimiento: Optimizar el rendimiento del sitio para una respuesta rápida a las solicitudes de los usuarios.

* Cumplimiento Normativo: Asegurar que el sitio cumple con todas las regulaciones y leyes aplicables, como las relacionadas con la privacidad y la seguridad de los datos.